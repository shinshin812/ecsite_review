<?php
require('function.php');
userCheckOk();
?>
<html>
<title>ユーザー登録</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .form-wrapper {
      background: #fafafa;
      margin: 1em auto;
      padding: 20 20px;
      width: 500px;
    }
    .text-center{
        font-weight: bold;
        font-size: 17px;
        margin: 20px 320px;

    }
    .form-control{
      margin-bottom: 10px;
    }
</style>
<body>
  <div class="container">
    <div class="row">
      <div class="text-center">
  <form method="post" action="">
    <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
      <div class="alert-danger" role="alert"><?php echo checkName() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkEmail() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkPassword() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkAddress() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkCredit() ?></div>
    <h1 class="text-info">ユーザー登録</h1>
    <div class="form-group">
      名前
    <input class="form-control" placeholder="Name" type="text" name="name" value="<?php echo displayName() ?>">
    </div>
    <div class="form-group">
      メールアドレス
    <input class="form-control" placeholder="Email" type="email" name="email" value="<?php echo displayEmail() ?>">
    </div>
    <div class="form-group">
      パスワード
    <input class="form-control" placeholder="Password" name="password" type="password" class="field" id="password" value="<?php echo displayPassword() ?>">
    <input name="check_password" type="checkbox" id="password-check">
    パスワードを表示する
    </div>
    <div class="form-group">
    郵便番号</br>
	<input type="text" placeholder="000-0000" name="zip11" size="10" maxlength="8" onKeyUp="AjaxZip3.zip2addr(this,'','address','address');">
    </div>
    <div class="form-group">
      住所
    <input class="form-control" placeholder="Address" type="text" name="address" value="<?php echo displayAddress() ?>">
    </div>
    <div class="form-group">
      クレジットカード番号
    <input class="form-control" placeholder="Credit" type="text" name="credit" value="<?php echo displayCredit() ?>">
    </div>
    <input class="btn btn-info btn-lg" type="submit" value="確認"></br>
  </form>
  </br>
  <a href="http://localhost/task/ecsite1/login.php">ログインはこちら</a>
        </div>
      </div>
    </div>
  </div>
</body>
<script>
 const pwd = document.getElementById('password');
 const pwdCheck = document.getElementById('password-check');
 pwdCheck.addEventListener('change', function() {
     if(pwdCheck.checked) {
         pwd.setAttribute('type', 'text');
     } else {
         pwd.setAttribute('type', 'password');
     }
 }, false);
 </script>
</html>
