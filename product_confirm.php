<?php
require('function.php');
$db = dbConnect();
 ?>
<html>
<title>商品登録確認</title>
<style>
      body{
        background: #e9e9e9;
        color: #5e5e5e;
      }
      .form-wrapper {
        background: #fafafa;
        margin: 3em auto;
        padding: 20 20px;
        width: 800px;
      }
    .text-center{
        font-weight: bold;
        font-size: 17px;
        margin: 30px 150px;
    }
    .form-item{
      margin-bottom: 15px;
    }
</style>
<body>
  <div class="container">
    <div class="row">
      <div class="text-center">
  <form method="post" action="product_complete.php" enctype="multipart/form-data">
    <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
    <div class="form-group">
      商品名</br>
    <?php echo $_SESSION['product_name'] ?>
    </div>
    <div class="form-group">
      カテゴリー</br>
    <?php echo $_SESSION['category'] ?>
    </div>
    <div class="form-group">
      画像</br>
    <img src="<?php echo "../../img/".$_SESSION['product_image']['name'] ?>">
    </div>
    <div class="form-group">
      画像詳細</br>
    <img src="<?php echo "../../img/".$_SESSION['image_detail1']['name'] ?>">
    <img src="<?php echo "../../img/".$_SESSION['image_detail2']['name'] ?>">
    <img src="<?php echo "../../img/".$_SESSION['image_detail3']['name'] ?>">
    <img src="<?php echo "../../img/".$_SESSION['image_detail4']['name'] ?>">
    </div>
    <div class="form-group">
      紹介文</br>
    <?php echo $_SESSION['product_introduction'] ?>
    </div>
    <div class="form-group">
      値段</br>
    <?php echo $_SESSION['price'] ?>
    </div>
    <div class="form-item">
    <input class="btn btn-info btn-lg" type="submit" value="送信">
    </div>
  </form>
  <form method="post" action="product_register.php">
  <input type="submit" value="戻る" name="back">
</form>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
