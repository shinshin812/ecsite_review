<?php
require('function.php');
$db = dbConnect();

    $inquiryName = $_SESSION['name'];
    $inquiryEmail = $_SESSION['email'];
    $inquiryTitle = $_SESSION['title'];
    $inquiryMessage = $_SESSION['message'];

    $sql="INSERT INTO inquiry (name, email, title, message) VALUES (:inquiryName, :inquiryEmail, :inquiryTitle, :inquiryMessage)";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':inquiryName',$inquiryName, PDO::PARAM_STR);
    $stmt->bindParam(':inquiryEmail',$inquiryEmail, PDO::PARAM_STR);
    $stmt->bindParam(':inquiryTitle',$inquiryTitle, PDO::PARAM_STR);
    $stmt->bindParam(':inquiryMessage',$inquiryMessage, PDO::PARAM_STR);
    $stmt->execute();
?>
<html>
<title>お問い合わせ完了</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .form-wrapper {
      background: #fafafa;
      margin: 3em auto;
      padding: 20 20px;
      width: 1100px;
    }
    .text-center{
        font-weight: bold;
        font-size: 17px;
        margin: 50px 20px;

    }
    .form-control{
      margin-bottom: 10px;
    }
    .font{
      color: #33CC00
    }
</style>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand fas" href="home.php">&#xf015;</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="product_list.php">LIST
              <span class="sr-only">(current)</span>
            </a>
          </li>
            <form method="post" action="cart.php">
              <input class="btn btn-primary btn-sm" type="submit" name="nakami" value="cart">
            </form>
            <li class="nav-item">
              <?php $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
                    $stmt->execute(array($_SESSION['name']));
                    foreach($stmt as $row): ?>
              <form method="post" name="form" action="favorite_list.php">
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
                <a class="nav-link" href="javascript:form.submit()">favorite</a>
              </form>
            </li>
          <?php endforeach ?>
            <li class="nav-item">
              <?php $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
                    $stmt->execute(array($_SESSION['name']));
                    foreach($stmt as $row): ?>
              <form method="post" name="form1" action="order_history.php">
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
                <a class="nav-link" href="javascript:form1.submit()">Order History</a>
              </form>
            </li>
            <?php endforeach ?>
          <li class="nav-item">
            <a class="nav-link" href="logout.php">Logout</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container">
    <div class="row">
      <div class="text-center">
    <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
    <h1 class="font">お問い合わせを送信いたしました。</h1>
    <h5>お問い合わせいただきありがとうございます。返信までしばらくお待ちください。</br>一週間経っても返事がない場合は、正しく受信できなかった可能性があるので、</br>恐れ入りますが再度のご連絡をお願いします。</br></br>今後ともどうぞよろしくお願い申し上げます。</h5>
  <input class="btn btn-link btn-block" type="button" value="戻る" onclick="location.href='product_list.php'">
        </div>
      </div>
    </div>
  </div>
</body>
</html>
