<?php
require('function.php');
if(empty($_SESSION['name'])){
  header('location: admin_login.php');
  exit();
}
$db = dbConnect();
?>
<html>
<title>ユーザー情報(管理者)</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .color{
      border-bottom: solid 3px #87CEFA;
    }
</style>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand fas" href="#">&#xf015;</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">HOME
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="admin_logout.php">Logout</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

<div class="container mt-5">
  <div class="text-center">
    <div class="row">
      <h1>ユーザー情報</h1>
      <?php $sql= "SELECT * FROM user";
            $stmt = $db->query($sql);
            foreach ($stmt as $row): ?>
<table class="table">
  <tbody>
    <tr>
      <td><p class="color">名前</p><?php echo $row['name'] ?></td>
      <td><p class="color">メールアドレス</p><?php echo $row['email'] ?></td>
      <td><p class="color">パスワード</p><?php echo $row['password'] ?></td>
      <td><p class="color">住所</p><?php echo $row['address'] ?></td>
      <td><p class="color">クレジットカード番号</p><?php echo $row['credit'] ?></td>
    </tr>
  </tbody>
</table>
<?php endforeach ?>
    </div>
  </div>
</div>

<footer class="py-5 bg-dark">
  <p class="m-0 text-center text-white">@God Mountain</p>
  </footer>
</body>
</html>
