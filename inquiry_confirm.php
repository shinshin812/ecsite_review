<?php
require('function.php');
$db = dbConnect();
?>
<html>
<title>お問い合わせ確認</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .form-wrapper {
      background: #fafafa;
      margin: 3em auto;
      padding: 20 20px;
      width: 1100px;
    }
    .text-center{
        font-weight: bold;
        font-size: 17px;
        margin: 50px 20px;

    }
    .form-control{
      margin-bottom: 10px;
    }
</style>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand fas" href="home.php">&#xf015;</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="product_list.php">LIST
              <span class="sr-only">(current)</span>
            </a>
          </li>
            <form method="post" action="cart.php">
              <input class="btn btn-primary btn-sm" type="submit" name="nakami" value="cart">
            </form>
            <li class="nav-item">
              <?php $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
                    $stmt->execute(array($_SESSION['name']));
                    foreach($stmt as $row): ?>
              <form method="post" name="form" action="favorite_list.php">
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
                <a class="nav-link" href="javascript:form.submit()">favorite</a>
              </form>
            </li>
          <?php endforeach ?>
            <li class="nav-item">
              <?php $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
                    $stmt->execute(array($_SESSION['name']));
                    foreach($stmt as $row): ?>
              <form method="post" name="form1" action="order_history.php">
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
                <a class="nav-link" href="javascript:form1.submit()">Order History</a>
              </form>
            </li>
            <?php endforeach ?>
          <li class="nav-item">
            <a class="nav-link" href="logout.php">Logout</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container">
    <div class="row">
      <div class="text-center">
  <form method="post" action="inquiry_complete.php">
    <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
    <h1>お問い合わせ</h1>
    <div class="form-group">
      名前</br>
    <?php echo $_SESSION['name'] ?>
    </div>
    <div class="form-group">
      メールアドレス</br>
    <?php echo $_SESSION['email'] ?>
    </div>
    <div class="form-group">
      題名</br>
    <?php echo $_SESSION['title'] ?>
    </div>
    <div class="form-group">
      本文</br>
    <?php echo $_SESSION['message'] ?>
    </div>
    <input class="btn btn-info btn-block" type="submit" value="送信"></br>
  </form>
  <input class="btn btn-link btn-block" type="button" value="戻る" onclick="location.href='inquiry_input.php'">
        </div>
      </div>
    </div>
  </div>
</body>
</html>
