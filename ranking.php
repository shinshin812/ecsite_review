<?php
require('function.php');
$db = dbConnect();

$sql = "SELECT product.id,product.product_name,product.product_image,product.image_detail1,product.image_detail2,product.image_detail3,product.image_detail4,product.price,product.product_introduction,sum(order_detail.num) FROM product INNER JOIN order_detail ON product.id = order_detail.product_id GROUP BY product_id ORDER BY sum(num) DESC LIMIT 5";
$stmt = $db->prepare($sql);
$stmt->execute();
 ?>
<html>
<title>売れ筋ランキング</title>
<style>
tr.rank{
height: 183px;
}
body{
  background: #e9e9e9;
  color: #5e5e5e;
}
.color{
  border-bottom: solid 3px #87CEFA;
}
.font{
  margin-top: 90px;
}
</style>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <div class="container">
    <a class="navbar-brand fas" href="home.php">&#xf015;</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="product_list.php">LIST
            <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="logout.php">Logout</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<body>
 <div class="font">
  <div class="container mt-5">
    <h2 class="color"><i class="fas fa-crown"></i> 売れ筋ランキングTOP5 <i class="fas fa-crown"></i></h2>
    <div class="text-center">
      <div class="row">
<table>
  <tr>
    <td>
<table class="rank" border=1>
<tr><th>順位</th></tr>
<?php for($i=1; $i<6; $i++): ?>
<tr class="rank"><th><?php echo $i ?></th></tr>
<?php endfor ?>
</table>
</td>
<td>
<table width="1000px" border=1>
<tr>
<th>商品名</th>
<th>画像</th>
<th>値段</th>
<th>紹介文</th>
<th>詳細</th>
</tr>
<?php foreach($stmt as $row): ?>
<tr>
<td><?php echo $row['product_name'] ?></td>
<td width="150" height="150"><img src="<?php echo "../../".$row['product_image'] ?>" width="180" height="180"></td>
<?php $tax = 1.1;
          $price = $row['price'] * $tax;
          $product_price = number_format($price); ?>
<td><?php echo $product_price ?>円(税込)</td>
<td><?php echo $row['product_introduction'] ?></td>
<form action="product_detail.php" method="POST">
<td><input class="btn btn-primary btn-sm" type="submit" name="detail" value="詳細"></td>
<input type="hidden" name="id" value="<?php echo $row['id']; ?>">
<input type="hidden" name="product_name" value="<?php echo $row['product_name'] ?>">
<input type="hidden" name="product_image" value="<?php echo "../../".$row['product_image'] ?>">
<input type="hidden" name="image_detail1" value="<?php echo "../../".$row['image_detail1'] ?>">
<input type="hidden" name="image_detail2" value="<?php echo "../../".$row['image_detail2'] ?>">
<input type="hidden" name="image_detail3" value="<?php echo "../../".$row['image_detail3'] ?>">
<input type="hidden" name="image_detail4" value="<?php echo "../../".$row['image_detail4'] ?>">
<input type="hidden" name="product_introduction" value="<?php echo $row['product_introduction'] ?>">
<input type="hidden" name="price" value="<?php echo $price ?>">
<?php if(!empty($_SESSION['name'])):
   $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
   $stmt->execute(array($_SESSION['name']));
   foreach($stmt as $row): ?>
  <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
  <?php endforeach ?>
  <?php endif ?>
</form>
</tr>
<?php endforeach ?>
</table>
</td>
</tr>
</table>
      </div>
    </div>
  </div>
</div>
<a href="product_list.php">商品一覧ページへ</a>
<footer class="py-5 bg-dark">
  <div class="container">
    <div class="text-center">
      <?php if(!empty($_SESSION['name'])): ?>
      <a class="text-white" href="inquiry_input.php">お問い合わせ</a>
    <?php endif ?>
    </div>
  </div>
  <p class="m-0 text-center text-white">@God Mountain</p>
  </footer>
</body>
</html>
