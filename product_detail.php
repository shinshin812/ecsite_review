<?php
require('function.php');
$db = dbConnect();

$sql1 = "SELECT * FROM review WHERE product_id = {$_POST['id']} ORDER BY id DESC";

if(isset($_POST['delete'])){
$sql = "DELETE FROM review WHERE id = :id";
$stmt = $db->prepare($sql);
$params = array(':id'=>$_POST['id']);
$stmt->execute($params);
header('Location: product_list.php');
exit();
}

    $userId = $_POST['user_id'];
    $favoriteId = $_POST['id'];
    $favoriteName = $_POST['product_name'];
    $favoriteImage = $_POST['product_image'];
    $favoritePrice = $_POST['price'];
    $favoriteIntroduction = $_POST['product_introduction'];

    $sql= "SELECT * FROM favorite WHERE EXISTS (SELECT * FROM favorite WHERE favorite_id = '$favoriteId' AND user_id = '$userId')";
    $stmt = $db->query($sql);
    $row1 = $stmt->fetch(PDO::FETCH_ASSOC);

if(isset($_POST['favorite'])){
    if($row1 == FALSE){
      $sql = "INSERT INTO favorite (user_id, favorite_id, favorite_name, favorite_image, favorite_price, favorite_introduction)VALUES (:userId,:favoriteId,:favoriteName,:favoriteImage, :favoritePrice, :favoriteIntroduction)";
      $stmt = $db->prepare($sql);
      $stmt->bindParam(':userId',$userId, PDO::PARAM_STR);
      $stmt->bindParam(':favoriteId',$favoriteId, PDO::PARAM_STR);
      $stmt->bindParam(':favoriteName',$favoriteName, PDO::PARAM_STR);
      $stmt->bindParam(':favoriteImage',$favoriteImage, PDO::PARAM_STR);
      $stmt->bindParam(':favoritePrice',$favoritePrice, PDO::PARAM_STR);
      $stmt->bindParam(':favoriteIntroduction',$favoriteIntroduction, PDO::PARAM_STR);
      $stmt->execute();
      header('location: product_list.php');
      exit();
      }
    }

    if(isset($_POST['favorite_delete'])){
      $sql = "DELETE FROM favorite WHERE favorite_id = :favorite_id AND user_id = :user_id";
      $stmt = $db->prepare($sql);
      $params = array(':favorite_id'=>$_POST['id'],':user_id'=>$_POST['user_id']);
      $stmt->execute($params);
    }
?>
<html>
<title>商品詳細</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .color{
      border-bottom: solid 1px #87CEFA;
    }
    #square-button {
  width: 80px;
  height: 80px;
  background: #232323;
}
#square-button.blue {
  background: #21759b;
}
table {
width: 1000px;
height: 150px
}
.cart-position{
  margin-top: 7px;
}
</style>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand fas" href="home.php">&#xf015;</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="product_list.php">LIST
              <span class="sr-only">(current)</span>
            </a>
          </li>
            <form method="post" action="cart.php">
              <div class="cart-position">
              <input class="btn btn-primary btn-sm fas" type="submit" name="nakami" value="&#xf07a; cart">
            </div>
            </form>
            <li class="nav-item">
              <?php if(!empty($_SESSION['name'])):
                    $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
                    $stmt->execute(array($_SESSION['name'])); ?>
                <form method="post" name="form" action="favorite_list.php">
                <a class="nav-link" href="javascript:form.submit()">favorite</a>
                <?php foreach($stmt as $row): ?>
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
              </form>
            </li>
          <?php endforeach ?>
        <?php elseif(empty($_SESSION['name'])):
                $sql = "SELECT * FROM user";
                $stmt = $db->prepare($sql);
                $stmt->execute(); ?>
                <form method="post" name="form" action="favorite_list.php">
                <a class="nav-link" href="javascript:form.submit()">favorite</a>
                <?php foreach($stmt as $row): ?>
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
              </form>
            </li>
          <?php endforeach ?>
        <?php endif ?>
            <li class="nav-item">
              <?php if(!empty($_SESSION['name'])):
                    $sql = "SELECT * FROM user";
                    $stmt = $db->prepare($sql);
                    $stmt->execute(); ?>
                <form method="post" name="form1" action="order_history.php">
                <a class="nav-link" href="javascript:form1.submit()">Order History</a>
                <?php foreach($stmt as $row): ?>
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
              </form>
            <?php endforeach ?>
            <?php endif ?>
            </li>
          <li class="nav-item">
            <?php if(!empty($_SESSION['name'])): ?>
            <a class="nav-link" href="logout.php">Logout</a>
          <?php elseif(empty($_SESSION['name'])): ?>
            <a class="nav-link" href="login.php">Login</a>
          <?php endif ?>
          </li>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <form method="post" action="cart.php">
    <div class="container mt-5">
      <div class="text-center">
        <div class="row">
          <div class="col-lg-8 mt-5">
            <div class="col-xs-12 col-lg-10 mt-5">
                <div class="card img-thumbnail">
                  <div class="wrap">
<p><img class="card-img-top" src="<?php echo $_POST['product_image'] ?>" id="MainPhoto"></p>
<img src="<?php echo $_POST['product_image'] ?>" width="110" class="ChangePhoto">
<img src="<?php echo $_POST['image_detail1'] ?>" width="110" class="ChangePhoto">
<img src="<?php echo $_POST['image_detail2'] ?>" width="110" class="ChangePhoto">
<img src="<?php echo $_POST['image_detail3'] ?>" width="110" class="ChangePhoto">
<img src="<?php echo $_POST['image_detail4'] ?>" width="110" class="ChangePhoto">
</div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="card bg-light mt-5 mb-5">
    <div class="col-lg-12 mt-5">
      <div class="col-xs-12 col-lg-12 mt-5">
        <div class="card-body">
          <p class="color">商品名</p>
         <h2 class="card-title">
      <?php echo $_POST['product_name'] ?>
    </h2></br>
    <p class="color">詳細</p>
      <h3><?php echo $_POST['product_introduction'] ?></h3></br>
      <p class="color">値段</p>
      <p><?php $price = number_format($_POST['price']);
                echo $price ?>円(税込)</p></br>
      <select name="number">
      <?php for($x=1; $x<=9; $x++){
        echo "<option>{$x}</option>";
      } ?></select></br>
    </div>
    <div class="card-footer">
  <input class="btn btn-primary btn-lg btn-block fas" type="submit" name="submit" value="&#xf07a; カートに入れる">
  <input type="hidden" name="id" value="<?php echo $_POST['id'] ?>">
  <input type="hidden" name="product_name" value="<?php echo $_POST['product_name'] ?>">
  <input type="hidden" name="product_image" value="<?php echo $_POST['product_image'] ?>">
  <input type="hidden" name="product_introduction" value="<?php echo $_POST['product_introduction'] ?>">
  <input type="hidden" name="price" value="<?php echo $_POST['price'] ?>">
</form>
</div></br>
<form method="post" action="">
  <?php if(!empty($_SESSION['name'])): ?>
    <?php if($row1 == FALSE || isset($_POST['favorite_delete'])): ?>
  <input class="btn btn-info btn-block far" type="submit" name="favorite" value="お気に入り&#xf004;">
  <input type="hidden" name="id" value="<?php echo $_POST['id'] ?>">
  <input type="hidden" name="product_name" value="<?php echo $_POST['product_name'] ?>">
  <input type="hidden" name="product_image" value="<?php echo $_POST['product_image'] ?>">
  <input type="hidden" name="product_introduction" value="<?php echo $_POST['product_introduction'] ?>">
  <input type="hidden" name="price" value="<?php echo $_POST['price'] ?>">
  <?php $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
        $stmt->execute(array($_SESSION['name']));
        foreach($stmt as $row): ?>
    <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
  <?php endforeach ?>
  <input type="hidden" name="image_detail1" value="<?php echo $_POST['image_detail1'] ?>">
  <input type="hidden" name="image_detail2" value="<?php echo $_POST['image_detail2'] ?>">
  <input type="hidden" name="image_detail3" value="<?php echo $_POST['image_detail3'] ?>">
  <input type="hidden" name="image_detail4" value="<?php echo $_POST['image_detail4'] ?>">
<?php elseif($row1 == TRUE): ?>
<input class="btn btn-info btn-block fas" type="submit" name="favorite_delete" value="お気に入り済み&#xf004;" onclick="return confirm('お気に入りから削除しますか？')">
<input type="hidden" name="id" value="<?php echo $row1['favorite_id'] ?>">
<input type="hidden" name="user_id" value="<?php echo $row1['user_id'] ?>">
<input type="hidden" name="id" value="<?php echo $_POST['id'] ?>">
<input type="hidden" name="product_name" value="<?php echo $_POST['product_name'] ?>">
<input type="hidden" name="product_image" value="<?php echo $_POST['product_image'] ?>">
<input type="hidden" name="product_introduction" value="<?php echo $_POST['product_introduction'] ?>">
<input type="hidden" name="price" value="<?php echo $_POST['price'] ?>">
<?php $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
      $stmt->execute(array($_SESSION['name']));
      foreach($stmt as $row): ?>
  <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
<?php endforeach ?>
<input type="hidden" name="image_detail1" value="<?php echo $_POST['image_detail1'] ?>">
<input type="hidden" name="image_detail2" value="<?php echo $_POST['image_detail2'] ?>">
<input type="hidden" name="image_detail3" value="<?php echo $_POST['image_detail3'] ?>">
<input type="hidden" name="image_detail4" value="<?php echo $_POST['image_detail4'] ?>">
<?php endif ?>
  <?php endif ?>
</form>
<form method="post" action="review.php">
  <input class="btn btn-primary btn-block" type="submit" name="review" value="口コミ投稿">
  <input type="hidden" name="id" value="<?php echo $_POST['id'] ?>">
  <?php if(!empty($_SESSION['name'])):
     $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
     $stmt->execute(array($_SESSION['name']));
     foreach($stmt as $row): ?>
    <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
    <?php endforeach ?>
    <?php endif ?>
</form>
<input class="btn btn-link btn-block" type="button" value="戻る" onclick="location.href='product_list.php'">
          </div>
        </div>
      </div>
    </div>
  </div>
  <h3>口コミ</h3>
  <table class="text-center table table-condensed table-striped">
  <tr>
  <th>投稿日</th>
  <th>ニックネーム</th>
  <th>コメント</th>
  <?php if(isset($_SESSION['name'])): ?>
  <th>削除</th>
<?php endif ?>
  </tr>
  <?php $stmt = $db->query($sql1);
    foreach ($stmt as $row): ?>
  <tr>
  <td><?php echo $row['date'] ?></td>
  <td><?php echo $row['nickname'] ?></td>
  <td><?php echo $row['comment'] ?></td>
  <form action="" method="POST">
    <?php if(isset($_SESSION['name'])):
    $stmt1 = $db->prepare("SELECT * FROM user WHERE name= ? ");
    $stmt1->execute(array($_SESSION['name']));
    foreach($stmt1 as $row1):
      if($row['user_id'] == $row1['user_id']): ?>
  <td><input class="btn btn-danger btn-sm" type="submit" name="delete" value="削除"></td>
  <input type="hidden" name="id" value="<?php echo $row['id'] ?>">
  </form>
  </tr>
<?php endif ?>
<?php endforeach ?>
<?php endif ?>
  <?php endforeach ?>
  </table>
</div>
</div>

<footer class="py-5 bg-dark">
  <div class="container">
    <div class="text-center">
      <?php if(!empty($_SESSION['name'])): ?>
      <a class="text-white" href="inquiry_input.php">お問い合わせ</a>
    <?php endif ?>
    </div>
  </div>
  <p class="m-0 text-center text-white">@God Mountain</p>
  </footer>
  <script>
  $(function(){
      $("img.ChangePhoto").click(function(){
          var ImgSrc = $(this).attr("src");
          var ImgAlt = $(this).attr("alt");
          $("img#MainPhoto").attr({src:ImgSrc,alt:ImgAlt});
          $("img#MainPhoto").hide();
          $("img#MainPhoto").fadeIn("slow");
          return false;
      });
  });
  </script>
</body>
</html>
