<?php
require('function.php');
$db = dbConnect();

$user_id = $_POST['user_id'];
$sql = "SELECT * FROM order_detail WHERE user_id = '$user_id'";
$stmt = $db->query($sql);
 ?>
<html>
<title>注文履歴</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    li{
      list-style: none;
    }
    .color{
      border-bottom: solid 1px #87CEFA;
    }
    .font{
      margin-top: 90px;
    }
</style>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand fas" href="home.php">&#xf015;</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="product_list.php">LIST
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout.php">Logout</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

<div class="font">
  <div class="container mt-5">
    <div class="text-center">
      <div class="row">
        <h1>注文履歴</h1>
  <?php if(!empty($_SESSION['name'])):
        foreach($stmt as $row): ?>
  <table class="table">
  	<tbody>
  		<tr>
        <ul>
  			<td><p class="color">注文日</p><li><?php echo $row['date'] ?></li></td>
  			<td><p class="color">注文番号</p><li><?php echo $row['order_id'] ?></li></td>
  			<td><p class="color">商品名</p><li><?php echo $row['product_name'] ?></li></td>
  			<td><p class="color">数量</p><li><?php echo $row['num'] ?></li></td>
  			<td><p class="color">小計</p><li><?php echo $row['sum'] ?>円(税込)</li></td>
        </ul>
  		</tr>
  	</tbody>
  </table>
<?php endforeach ?>
      </div>
    </div>
  </div>
</div>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
<?php elseif(empty($_SESSION['name'])):
  header('location: login.php');
  exit();
  endif ?>

<footer class="py-5 bg-dark fixed-bottom">
  <div class="container">
    <div class="text-center">
      <?php if(!empty($_SESSION['name'])): ?>
      <a class="text-white" href="inquiry_input.php">お問い合わせ</a>
    <?php endif ?>
    </div>
  </div>
  <p class="m-0 text-center text-white">@God Mountain</p>
  </footer>
</body>
</html>
