<?php
require('function.php');
$db = dbConnect();

if($_SERVER['REQUEST_METHOD']==='POST'){
		header('location: cart.php');
	}

if(isset($_POST['submit'])){
@$id = $_POST['id'];
if(!isset($_SESSION['cart'][$id]['product_name']) && !isset($_POST['nakami'])){
  if(!isset($_POST['back'])){
  $_SESSION['cart'][$id] = $_POST;
}
  }elseif(isset($_POST['submit'])){
    $_SESSION['cart'][$id]['number'] += $_POST['number'];
  }
}

$sum = 0;

if(!empty($_SESSION['name'])){
$stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
$stmt->execute(array($_SESSION['name']));
}elseif(empty($_SESSION['name'])){
  $sql = "SELECT * FROM user";
  $stmt = $db->prepare($sql);
  $stmt->execute();
}

if(!empty($_SESSION['name'])){
if(isset($_POST['submit'])){
  $sql = "DELETE FROM favorite WHERE favorite_id = :favorite_id AND user_id = :user_id";
  $stmt = $db->prepare($sql);
  $params = array(':favorite_id'=>$_POST['id'],':user_id'=>$_POST['user_id']);
  $stmt->execute($params);
  }
}
?>
<html>
<title>カート一覧</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    li{
      list-style: none;
    }
    .color{
      border-bottom: solid 1px #87CEFA;
    }
		.font{
			margin-top: 90px;
		}
</style>
<body>
  <?php if(!empty($_SESSION['cart'])): ?>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand fas" href="home.php">&#xf015;</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="product_list.php">LIST
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <form method="post" action="order_confirm.php">
            <input class="btn btn-primary btn-sm" type="submit" value="pay">
            <?php foreach($stmt as $row): ?>
            <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
            <input type="hidden" name="email" value="<?php echo $row['email'] ?>">
            <input type="hidden" name="password" value="<?php echo $row['password'] ?>">
            <input type="hidden" name="address" value="<?php echo $row['address'] ?>">
            <input type="hidden" name="credit" value="<?php echo $row['credit'] ?>">
            <?php endforeach ?>
            </form>
            <li class="nav-item">
              <?php if(!empty($_SESSION['name'])):
                    $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
                    $stmt->execute(array($_SESSION['name'])); ?>
                <form method="post" name="form" action="favorite_list.php">
                <a class="nav-link" href="javascript:form.submit()">favorite</a>
                <?php foreach($stmt as $row): ?>
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
              </form>
            </li>
          <?php endforeach ?>
        <?php elseif(empty($_SESSION['name'])):
                $sql = "SELECT * FROM user";
                $stmt = $db->prepare($sql);
                $stmt->execute(); ?>
                <form method="post" name="form" action="favorite_list.php">
                <a class="nav-link" href="javascript:form.submit()">favorite</a>
                <?php foreach($stmt as $row): ?>
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
              </form>
            </li>
          <?php endforeach ?>
        <?php endif ?>
          <li class="nav-item">
            <form method="post" name="form1" action="order_history.php">
              <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
              <a class="nav-link" href="javascript:form1.submit()">Order History</a>
            </form>
          </li>
          <li class="nav-item">
            <?php if(!empty($_SESSION['name'])): ?>
            <a class="nav-link" href="logout.php">Logout</a>
          <?php elseif(empty($_SESSION['name'])): ?>
            <a class="nav-link" href="login.php">Login</a>
          <?php endif ?>
          </li>
        </ul>
      </div>
    </div>
  </nav>
<?php endif ?>

<div class="font">
  <div class="container mt-5">
    <div class="text-center">
    <div class="row">
  <?php if(!empty($_SESSION['cart'])): ?>
    <?php foreach($_SESSION['cart'] as $key => $value): ?>
    <?php $sum += $value['price'] * $value['number']; ?>
    <table class="table">
    <tr>
      <td rowspan="3"><img src="<?php echo $value['product_image'] ?>" width="200" height="200"></td>
        <ul>
      <td><p class="color">商品名</p><li><?php echo $value['product_name'] ?></li></td>
      <td rowspan="3"><p class="color">数量</p><li><?php echo $value['number'] ?></li></td>
      <td rowspan="3"><p class="color">小計</p><li><?php $gokei = $value['price'] * $value['number'];
                $product_gokei = number_format($gokei);
                echo $product_gokei; ?>円(税込)</li></td>
      <td rowspan="3"><form method="post" action="">
      <input class="btn btn-danger btn-block" type="submit" name="delete" value="削除" onclick="return confirm('本当に削除してよろしいですか？')">
      <input type="hidden" name="id" value="<?php echo $value['id'] ?>">
      <?php
        if(isset($_POST['delete'])){
          unset($_SESSION['cart'][$_POST['id']]);
        }
       ?>
    </form></td>
    </tr>
    <tr>
      <td><p class="color">詳細</p><li><?php echo $value['product_introduction'] ?></li></td>
    </tr>
    <tr>
      <td><p class="color">値段</p><li><?php $price = number_format($value['price']);
              echo $price ?>円(税込)</li></td>
      </ul>
    </tr>
  </table>
  <?php endforeach ?>
			</div>
		</div>
	</div>
</div>
  <table class="table">
    <tr>
      <th class="text-center">カート全体の合計金額</th>
    </tr>
    <tr>
      <td class="text-center"><?php $product_sum = number_format($sum);
                echo $product_sum; ?>円(税込)</td>
    </tr>
    <tr>
			<?php
			$sql = "SELECT * FROM user";
      $stmt = $db->prepare($sql);
      $stmt->execute();
			 ?>
      <form method="post" action="order_confirm.php">
        <td><input class="btn btn-primary btn-block" type="submit" value="購入手続きに進む"></td>
        <?php foreach($stmt as $row): ?>
        <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
        <input type="hidden" name="email" value="<?php echo $row['email'] ?>">
        <input type="hidden" name="password" value="<?php echo $row['password'] ?>">
        <input type="hidden" name="address" value="<?php echo $row['address'] ?>">
        <input type="hidden" name="credit" value="<?php echo $row['credit'] ?>">
      </form>
      <?php endforeach ?>
    </tr>
    <tr>
      <form method="post" action="">
        <td><input class="btn btn-danger btn-block" type="submit" name="all_delete" value="カートの中身を全て削除" onclick="return confirm('本当に削除してよろしいですか？')"></td>
        <?php if(isset($_POST['all_delete'])){
           unset($_SESSION['cart']);
				} ?>
      </form>
    </tr>
    <tr>
      <td><input class="btn btn-success btn-block" type="submit" value="買い物を続ける" onclick="location.href='product_list.php'"></td>
    </tr>
  </table>
<?php elseif(empty($_SESSION['cart'])): ?>
  <div class="text-center">
  <h1 style="margin: 70px 380px;">カートの中身は空です</h1>
  <input class="btn btn-info btn-lg" type="submit" value="戻る" onclick="location.href='product_list.php'">
</div>
<?php endif ?>

<?php if(!empty($_SESSION['cart'])): ?>
  <footer class="py-5 bg-dark">
    <div class="container">
      <div class="text-center">
        <?php if(!empty($_SESSION['name'])): ?>
        <a class="text-white" href="inquiry_input.php">お問い合わせ</a>
      <?php endif ?>
      </div>
    </div>
    <p class="m-0 text-center text-white">@God Mountain</p>
    </footer>
<?php endif ?>
</body>
</html>
