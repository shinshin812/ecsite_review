<?php
require('function.php');
$db = dbConnect();

date_default_timezone_set('Asia/Tokyo');

$userId = $_POST['user_id'];
$total = $_POST['product_sum'];
$address = $_POST['address'];
$credit = $_POST['credit'];
$date = date('Y-m-d');

$sql = "INSERT INTO `order` (user_id, total, address, credit, date)VALUES (:userId,:total,:address,:credit,:date)";
$stmt = $db->prepare($sql);
$stmt->bindParam(':userId',$userId, PDO::PARAM_STR);
$stmt->bindParam(':total',$total, PDO::PARAM_STR);
$stmt->bindParam(':address',$address, PDO::PARAM_STR);
$stmt->bindParam(':credit',$credit, PDO::PARAM_STR);
$stmt->bindParam(':date',$date, PDO::PARAM_STR);
$stmt->execute();

$_POST['order_id'] = $db->lastInsertId();
$orderId = $_POST['order_id'];

foreach($_SESSION['cart'] as $key => $value){
  $productId = $value['id'];
  $num = $value['number'];
  $product_name = $value['product_name'];
  $gokei = $value['price'] * $value['number'];
  $sum = number_format($gokei);
$sql = "INSERT INTO order_detail (user_id, order_id, product_id, product_name, num, sum,date)VALUES (:userId, :orderId, :productId, :product_name, :num, :sum,:date)";
$stmt = $db->prepare($sql);
$stmt->bindParam(':userId',$userId, PDO::PARAM_STR);
$stmt->bindParam(':orderId',$orderId, PDO::PARAM_STR);
$stmt->bindParam(':productId',$productId, PDO::PARAM_STR);
$stmt->bindParam(':product_name',$product_name, PDO::PARAM_STR);
$stmt->bindParam(':num',$num, PDO::PARAM_STR);
$stmt->bindParam(':sum',$sum, PDO::PARAM_STR);
$stmt->bindParam(':date',$date, PDO::PARAM_STR);
$stmt->execute();
}

if(isset($_POST['confirm'])){
  $_SESSION['cart'] = array();
}
?>
<html>
<title>注文完了</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .text-center{
        font-weight: bold;
        font-size: 20px;
        margin: 70px 320px;
    }
</style>
<body>
  <div class="text-center">
  <h1 class="form-signin-heading">注文完了しました</h1>
  <?php
  mb_language("Japanese");
  mb_internal_encoding("UTF-8");
  $to = "shinshintest34@gmail.com";
  $title = "商品購入完了";
  $content = "商品の購入が完了しました。またのご利用お待ちしております！";
  if (mb_send_mail($to, $title, $content)) {
    echo "メールが送信されました。";
    } else {
      echo "メールの送信に失敗しました。";
      }
  ?>
</br>
  <a href="http://localhost/task/ecsite1/product_list.php">商品一覧画面へ</a>
</div>
</body>
</html>
