<?php
require('function.php');
$db = dbConnect();

if(empty($_SESSION['name'])){
  header('location: login.php');
  exit();
}

if(isset($_POST['nickname']) && isset($_POST['comment'])){
  if($_POST['nickname'] != "" && $_POST['comment'] != ""){
    if(mb_strlen($_POST['nickname'])<=10 && mb_strlen($_POST['comment'])<=30){
if(isset($_POST['post'])){
$nickname = $_POST['nickname'];
$comment = $_POST['comment'];
$user_id = $_POST['user_id'];
$product_id = $_POST['id'];
$date = date('Y/m/d');
$sql = "INSERT INTO review(nickname, comment, user_id, product_id, date)VALUES(:nickname, :comment, :user_id, :product_id, :date)";
$stmt = $db->prepare($sql);
$stmt->bindParam(':nickname', $nickname, PDO::PARAM_STR);
$stmt->bindParam(':comment', $comment, PDO::PARAM_STR);
$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
$stmt->bindParam(':product_id', $product_id, PDO::PARAM_INT);
$stmt->bindParam(':date', $date, PDO::PARAM_STR);
$stmt->execute();
header('Location: product_list.php');
exit();
      }
    }
  }
}

?>
<html>
<title>口コミ投稿</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .form-wrapper {
      background: #fafafa;
      margin: 1em auto;
      padding: 20 20px;
      width: 500px;
    }
    .text-center{
        font-weight: bold;
        font-size: 17px;
        margin: 20px 320px;

    }
    .form-control{
      margin-bottom: 10px;
    }
</style>
<body>
  <div class="container">
    <div class="row">
      <div class="text-center">
  <form action="" method="post">
    <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
      <div class="alert-danger" role="alert"><?php echo checkNickname() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkComment() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkCountNickName() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkCountComment() ?></div>
    <h1 class="text-primary">口コミ投稿</h1>
    <div class="form-group">
      ニックネーム
    <input class="form-control" placeholder="nickname" type="text" name="nickname" value="<?php echo displayNickname() ?>">
    </div>
    <div class="form-group">
      コメント
    <textarea class="form-control" rows="5" placeholder="comment" type="text" name="comment" value="<?php echo displayComment() ?>"></textarea>
    </div>
    <input class="btn btn-info btn-block" type="submit" name="post" value="投稿"></br>
    <input type="hidden" name="user_id" value="<?php echo $_POST['user_id'] ?>">
    <input type="hidden" name="id" value="<?php echo $_POST['id'] ?>">
    <input class="btn btn-link btn-block" type="button" value="商品一覧へ戻る" onclick="location.href='product_list.php'">
        </div>
      </div>
    </div>
  </div>
</form>
</body>
</html>
