<?php
require('function.php');

$db = dbConnect();

if(isset($_POST['login']) && !empty($_POST['name'])){

$stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
$stmt->execute(array($_POST['name']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);

$hash = password_hash("{$_POST['password']}", PASSWORD_DEFAULT);

if(isset($_POST['name']) && isset($_POST['email'])){
  if(isset($_POST['password'])){
    if($_POST['name'] != $row['name'] || $_POST['email'] != $row['email'] || $_POST['password'] != $row['password']){
        $loginCheck = "※入力が間違っています";
    }
  }
}

if(empty($_SESSION['cart'])){
if(isset($_POST['email']) && isset($_POST['password'])){
  if($_POST['email'] == $row['email']){
    if(password_verify($_POST['password'],$row['password'])){
      if($_SESSION['name'] = $row['name']){
        header('location: home.php');
        exit();
        }
      }
    }
  }
}
if(!empty($_SESSION['cart'])){
if(isset($_POST['email']) && isset($_POST['password'])){
  if($_POST['email'] == $row['email']){
    if(password_verify($_POST['password'],$row['password'])){
      if($_SESSION['name'] = $row['name']){
        header('location: cart.php');
        exit();
        }
      }
    }
  }
}

}
?>
<html>
<title>ログイン</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
      background-image: url("../../img/NSW_SNKRS_app_JP_PC_p2.png");
      background-size: cover;
      background-attachment: fixed;
    }
    .form-wrapper {
      background: #fafafa;
      /* background: rgba(255,255,255,0.5); */
      margin: 3em auto;
      padding: 20 20px;
      width: 500px;
    }
    .text-center{
        font-weight: bold;
        font-size: 17px;
        margin: 90px 630px;
    }
    .form-control{
      margin-bottom: 15px;
    }
</style>
<body>
  <div class="container">
    <div class="row">
      <div class="text-center">
  <form method="post" action="">
    <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
      <div class="alert-danger" role="alert"><?php echo checkName() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkEmail() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkPassword() ?></div>
      <?php if(isset($loginCheck)): ?>
      <div class="alert-danger" role="alert"><?php echo $loginCheck ?></div>
    <?php endif ?>
    <h1 class="text-primary">ログイン</h1>
    <div class="form-group">
      名前
    <input class="form-control" placeholder="Name" type="text" name="name" value="<?php echo displayName() ?>">
    </div>
    <div class="form-group">
      メールアドレス
    <input class="form-control" placeholder="Email" type="text" name="email" value="<?php echo displayEmail() ?>">
    </div>
    <div class="form-group">
      パスワード
    <input class="form-control" placeholder="Password" name="password" type="password" class="field" id="password" value="<?php echo displayPassword() ?>">
    <input name="check_password" type="checkbox" id="password-check">
    パスワードを表示する
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit" name=login>ログイン</button>
  </form>
</br>
  <a href="http://localhost/task/ecsite1/user_register.php">登録はこちら</a></br>
  <a href="http://localhost/task/ecsite1/product_list.php">商品一覧へ</a>
        </div>
      </div>
    </div>
  </div>
</body>
<script>
 const pwd = document.getElementById('password');
 const pwdCheck = document.getElementById('password-check');
 pwdCheck.addEventListener('change', function() {
     if(pwdCheck.checked) {
         pwd.setAttribute('type', 'text');
     } else {
         pwd.setAttribute('type', 'password');
     }
 }, false);
 </script>
</html>
