<?php
require('function.php');
?>
<html>
<title>管理者登録確認</title>
<style>
body{
  background: #e9e9e9;
  color: #5e5e5e;
}
    .form-wrapper {
      background: #fafafa;
      margin: 3em auto;
      padding: 20 20px;
      width: 500px;
    }
    .text-center{
        font-weight: bold;
        font-size: 20px;
        margin: 70px 320px;
    }
    .form-item{
      margin-bottom: 15px;
    }
</style>
<body>
  <div class="container">
    <div class="row">
      <div class="text-center">
  <form method="post" action="admin_complete.php">
    <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
  <div class="form-group">
    名前</br>
  <?php echo $_SESSION['name'] ?>
  </div>
  <div class="form-group">
    メールアドレス</br>
  <?php echo $_SESSION['email'] ?>
  </div>
  <div class="form-group">
    パスワード</br>
  <?php echo $_SESSION['password'] ?>
  </div>
  <div class="form-item">
  <input class="btn btn-dark btn-lg" type="submit" value="送信">
  </div>
</form>
<input type="button" value="戻る" onclick="location.href='admin_register.php'">
        </div>
      </div>
    </div>
  </div>
</body>
</html>
