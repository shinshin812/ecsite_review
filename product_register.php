<?php
require('function.php');
checkOk();

if(isset($_POST['back'])){
  unlink("../../img/".$_SESSION['product_image']['name']);
  unlink("../../img/".$_SESSION['image_detail1']['name']);
  unlink("../../img/".$_SESSION['image_detail2']['name']);
  unlink("../../img/".$_SESSION['image_detail3']['name']);
  unlink("../../img/".$_SESSION['image_detail4']['name']);
}
 ?>
<html>
<title>商品登録</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .form-wrapper {
      background: #fafafa;
      margin: 3em auto;
      padding: 20 20px;
      width: 500px;
    }
    .text-center{
        font-weight: bold;
        font-size: 17px;
        margin: 90px 320px;
    }
    .form-control{
      margin-bottom: 10px;
    }
</style>
<body>
  <div class="container">
    <div class="row">
      <div class="text-center">
        <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
          <div class="alert-danger" role="alert"><?php echo checkProductName() ?></div>
          <div class="alert-danger" role="alert"><?php echo checkCategory() ?></div>
          <div class="alert-danger" role="alert"><?php echo checkProductIntroduction() ?></div>
          <div class="alert-danger" role="alert"><?php echo checkPrice() ?></div>
<h1 class="text-info">商品登録</h1>
<form method="post" action="" enctype="multipart/form-data">
  <div class="form-group">
    商品名
  <input class="form-control" placeholder="Product Name" type="text" name="product_name" value="<?php displayProductName() ?>">
  </div>
  <div class="form-group">
    カテゴリー
  <input class="form-control" placeholder="Category" type="text" name="category" value="<?php displayCategory() ?>">
  </div>
  <div class="form-group">
    画像</br>
  <input type="file" name="product_image">
  </div>
  <div class="form-group">
    画像詳細</br>
  <input type="file" name="image_detail1">
  <input type="file" name="image_detail2">
  <input type="file" name="image_detail3">
  <input type="file" name="image_detail4">
  </div>
  <div class="form-group">
    紹介文
  <textarea class="form-control" placeholder="Introduction" name="product_introduction"><?php displayProductIntroduction() ?></textarea>
  </div>
  <div class="form-group">
    値段
  <input class="form-control" placeholder="Price" type="text" name="price" value="<?php displayPrice() ?>">
  </div>
  <input class="btn btn-info btn-lg" type="submit" value="確認">
</form>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
