<?php
require('function.php');
$db = dbConnect();

if(empty($_SESSION['name'])){
  header('location: login.php');
  exit();
}

$sum = 0;
 ?>
<html>
<title>注文確認</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    li{
      list-style: none;
    }
    .color{
      border-bottom: solid 1px #87CEFA;
    }
    .user-private{
      margin-top: 80px;
    }
</style>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand fas" href="home.php">&#xf015;</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="product_list.php">LIST
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <?php $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
                  $stmt->execute(array($_SESSION['name']));
                  foreach($stmt as $row): ?>
            <form method="post" name="form" action="favorite_list.php">
              <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
              <a class="nav-link" href="javascript:form.submit()">favorite</a>
            </form>
          </li>
        <?php endforeach ?>
          <li class="nav-item">
            <form method="post" name="form1" action="order_history.php">
              <input type="hidden" name="user_id" value="<?php echo $_POST['user_id'] ?>">
              <a class="nav-link" href="javascript:form1.submit()">Order History</a>
          </form>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout.php">Logout</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

<div class="user-private">
  <div class="container mt-5">
    <div class="text-center">
    名前<p class="form-control"><?php echo $_SESSION['name'] ?></p>
    住所<p class="form-control"><?php echo $_POST['address'] ?></p>
    クレジットカード情報<p class="form-control"><?php echo $_POST['credit'] ?>
    </div>
  </div>
</div>

    <div class="container mt-5">
      <div class="text-center">
        <div class="row">
    <?php foreach($_SESSION['cart'] as $key => $value): ?>
    <?php $sum += $value['price'] * $value['number']; ?>
    <table class="table">
    <tr>
      <td rowspan="3"><img src="<?php echo $value['product_image'] ?>" width="200" height="200"></td>
        <ul>
      <td><p class="color">商品名</p><li><?php echo $value['product_name'] ?></li></td>
      <td rowspan="3"><p class="color">数量</p><li><?php echo $value['number'] ?></li></td>
      <td rowspan="3"><p class="color">小計</p><li><?php $gokei = $value['price'] * $value['number'];
                $product_gokei = number_format($gokei);
                echo $product_gokei; ?>円(税込)</li></td>
      <td rowspan="3"><form method="post" action="">
      <input class="btn btn-danger btn-block" type="submit" name="delate" value="削除" onclick="return confirm('本当に削除してよろしいですか？')">
      <input type="hidden" name="id" value="<?php echo $value['id'] ?>">
      <?php
        if(isset($_POST['delate'])){
          unset($_SESSION['cart'][$_POST['id']]);
          header('location: product_list.php');
          exit();
        }
       ?>
    </form></td>
    </tr>
    <tr>
      <td><p class="color">詳細</p><li><?php echo $value['product_introduction'] ?></li></td>
    </tr>
    <tr>
      <td><p class="color">値段</p><li><?php $price = number_format($value['price']);
              echo $price ?>円(税込)</li></td>
      </ul>
    </tr>
  </table>
  <?php endforeach ?>
    </div>
  </div>
</div>

  <table class="table">
    <tr>
      <th>カート全体の合計金額</th>
    </tr>
    <tr>
      <td class="text-center"><?php $product_sum = number_format($sum);
                echo $product_sum; ?>円(税込)</td>
    </tr>
    <tr>
  <form method="post" action="order_complete.php">
  <td><input class="btn btn-primary btn-lg btn-block" type="submit" name="confirm" value="注文を確定する"></td>
  <input type="hidden" name="product_sum" value="<?php echo $product_sum ?>">
  <input type="hidden" name="user_id" value="<?php echo $_POST['user_id'] ?>">
  <input type="hidden" name="address" value="<?php echo $_POST['address'] ?>">
  <input type="hidden" name="credit" value="<?php echo $_POST['credit'] ?>">
</form>
</tr>
<tr>
  <form method="post" action="cart.php">
    <td><input class="btn btn-link btn-block" type="submit" value="戻る" name="back"></td>
  </form>
</tr>
  </table>

  <footer class="py-5 bg-dark">
    <div class="container">
      <div class="text-center">
        <?php if(!empty($_SESSION['name'])): ?>
        <a class="text-white" href="inquiry_input.php">お問い合わせ</a>
      <?php endif ?>
      </div>
    </div>
    <p class="m-0 text-center text-white">@God Mountain</p>
    </footer>
</body>
</html>
