<?php
require('function.php');

$db = dbConnect();

if(isset($_POST['login']) && !empty($_POST['name'])){
$stmt = $db->prepare("SELECT * FROM admin WHERE admin_name= ? ");
$stmt->execute(array($_POST['name']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);

$hash = password_hash("{$_POST['password']}", PASSWORD_DEFAULT);

if(isset($_POST['name']) && isset($_POST['email'])){
  if(isset($_POST['password'])){
    if($_POST['name'] != $row['admin_name'] || $_POST['email'] != $row['admin_email'] || $_POST['password'] != $row['admin_password']){
        $loginCheck = "※入力が間違っています";
    }
  }
}

if(isset($_POST['email']) && isset($_POST['password'])){
  if($_POST['email'] == $row['admin_email']){
    if(password_verify($_POST['password'],$row['admin_password'])){
      if($_SESSION['name'] = $row['admin_name']){
        header('location: index.php');
        exit();
      }
    }
  }
}

}
?>
<html>
<title>管理者ログイン</title>
<style>
body{
  background: #e9e9e9;
  color: #5e5e5e;
}
.bg-image{
  background-image: url("../../img/HNCK4963.jpg");
  background-size: cover;
}
.bg-mask {
  background: rgba(255,255,255,0.5);
}
.form-wrapper {
  background: #fafafa;
  margin: 2em auto;
  padding: 90 20px;
  width: 500px;
}
.text-center{
    font-weight: bold;
    font-size: 17px;
    margin: 20px 320px;

}
.form-control{
  margin-bottom: 10px;
}
</style>
<body>
  <div class="bg-image">
    <div class="bg-mask">
      <div class="container">
        <div class="row">
          <div class="text-center">
  <form method="post" action="">
    <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
      <div class="alert-danger" role="alert"><?php echo checkName() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkEmail() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkPassword() ?></div>
      <?php if(isset($loginCheck)): ?>
      <div class="alert-danger" role="alert"><?php echo $loginCheck ?></div>
    <?php endif ?>
    <h1 class="text-primary">ログイン</h1>
    <div class="form-group">
      名前
    <input class="form-control" placeholder="Name" type="text" name="name" value="<?php echo displayName() ?>">
    </div>
    <div class="form-group">
      メールアドレス
    <input class="form-control" placeholder="Email" type="text" name="email" value="<?php echo displayEmail() ?>">
    </div>
    <div class="form-group">
      パスワード
    <input class="form-control" placeholder="Password" name="password" type="password" class="field" id="password" value="<?php echo displayPassword() ?>">
    <input name="check_password" type="checkbox" id="password-check">
    パスワードを表示する
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit" name=login>ログイン</button>
  </form>
</br>
  <a href="http://localhost/task/ecsite1/login.php">ユーザーログインはこちら</a>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</body>
<script>
 const pwd = document.getElementById('password');
 const pwdCheck = document.getElementById('password-check');
 pwdCheck.addEventListener('change', function() {
     if(pwdCheck.checked) {
         pwd.setAttribute('type', 'text');
     } else {
         pwd.setAttribute('type', 'password');
     }
 }, false);
 </script>
</html>
