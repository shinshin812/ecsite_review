<?php
require('function.php');
$db = dbConnect();

if(isset($_POST['delete'])){
  $sql = "DELETE FROM favorite WHERE favorite_id = :favorite_id AND user_id = :user_id";
  $stmt = $db->prepare($sql);
  $params = array(':favorite_id'=>$_POST['id'],':user_id'=>$_POST['user_id']);
  $stmt->execute($params);
  header('location: product_list.php');
  exit();
}

 ?>
