<?php
require('function.php');
$db = dbConnect();

$stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
$stmt->execute(array($_SESSION['name']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);

    if(isset($_POST['name']) && isset($_POST['email'])){
      if(isset($_POST['message']) && isset($_POST['title'])){
        if($_POST['name'] != $row['name'] || $_POST['email'] != $row['email']){
            $loginCheck = "※入力が間違っています";
        }
      }
    }

    if(isset($_POST['email']) && isset($_POST['message']) && isset($_POST['title'])){
      if($_POST['message'] != "" && $_POST['title'] != ""){
      if($_POST['email'] == $row['email']){
          if($_SESSION['name'] == $row['name']){
            $_SESSION = $_POST;
            header('location: inquiry_confirm.php');
            exit();
          }
        }
      }
    }
 ?>
<html>
<title>お問い合わせ</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .form-wrapper {
      background: #fafafa;
      margin: 3em auto;
      padding: 20 20px;
      width: 1100px;
    }
    .text-center{
        font-weight: bold;
        font-size: 17px;
        margin: 50px 20px;

    }
    .form-control{
      margin-bottom: 10px;
    }
</style>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand fas" href="home.php">&#xf015;</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="product_list.php">LIST
              <span class="sr-only">(current)</span>
            </a>
          </li>
            <form method="post" action="cart.php">
              <input class="btn btn-primary btn-sm" type="submit" name="nakami" value="cart">
            </form>
            <li class="nav-item">
              <?php if(!empty($_SESSION['name'])):
                    $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
                    $stmt->execute(array($_SESSION['name'])); ?>
                <form method="post" name="form" action="favorite_list.php">
                <a class="nav-link" href="javascript:form.submit()">favorite</a>
                <?php foreach($stmt as $row): ?>
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
              </form>
            </li>
          <?php endforeach ?>
        <?php elseif(empty($_SESSION['name'])):
                $sql = "SELECT * FROM user";
                $stmt = $db->prepare($sql);
                $stmt->execute(); ?>
                <form method="post" name="form" action="favorite_list.php">
                <a class="nav-link" href="javascript:form.submit()">favorite</a>
                <?php foreach($stmt as $row): ?>
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
              </form>
            </li>
          <?php endforeach ?>
        <?php endif ?>
            <li class="nav-item">
              <?php if(!empty($_SESSION['name'])):
                    $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
                    $stmt->execute(array($_SESSION['name'])); ?>
                <form method="post" name="form1" action="order_history.php">
                <a class="nav-link" href="javascript:form1.submit()">Order History</a>
                <?php foreach($stmt as $row): ?>
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
              </form>
            </li>
            <?php endforeach ?>
          <?php elseif(empty($_SESSION['name'])):
                  $sql = "SELECT * FROM user";
                  $stmt = $db->prepare($sql);
                  $stmt->execute(); ?>
              <form method="post" name="form1" action="order_history.php">
              <a class="nav-link" href="javascript:form1.submit()">Order History</a>
              <?php foreach($stmt as $row): ?>
              <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
            </form>
          </li>
          <?php endforeach ?>
          <?php endif ?>
          <li class="nav-item">
            <?php if(!empty($_SESSION['name'])): ?>
            <a class="nav-link" href="logout.php">Logout</a>
          <?php elseif(empty($_SESSION['name'])): ?>
            <a class="nav-link" href="login.php">Login</a>
          <?php endif ?>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container">
    <div class="row">
      <div class="text-center">
  <form method="post" action="">
    <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
      <div class="alert-danger" role="alert"><?php echo checkName() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkEmail() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkMessage() ?></div>
      <div class="alert-danger" role="alert"><?php echo checkTitle() ?></div>
      <?php if(isset($loginCheck)): ?>
      <div class="alert-danger" role="alert"><?php echo $loginCheck ?></div>
    <?php endif ?>
    <h1>お問い合わせ</h1>
    <div class="form-group">
      名前
    <input class="form-control" placeholder="Name" type="text" name="name" value="<?php echo displayName() ?>">
    </div>
    <div class="form-group">
      メールアドレス
    <input class="form-control" placeholder="Email" type="email" name="email" value="<?php echo displayEmail() ?>">
    </div>
    <div class="form-group">
      題名
    <input class="form-control" placeholder="Title" type="text" name="title" value="<?php echo displayTitle() ?>">
    </div>
    <div class="form-group">
      本文
    <textarea class="form-control" rows="5" placeholder="Message" type="text" name="message" value="<?php echo displayMessage() ?>"></textarea>
    </div>
    <input class="btn btn-info btn-block" type="submit" value="確認"></br>
  </form>
  </br>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
