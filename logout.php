<?php
session_start();

$_SESSION['name'] = array();
session_destroy();
 ?>
 <html>
 <title>ログアウト</title>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 <style>
     body{
       background: #e9e9e9;
       color: #5e5e5e;
     }
     .text-center{
         font-weight: bold;
         font-size: 20px;
         margin: 70px 320px;
     }
 </style>
 <body>
 <div class="text-center">
   <h2>ログアウトしました</h2>
   <input class="btn btn-info btn-lg" type="submit" value="戻る" onclick="location.href='home.php'">
 </div>
 </body>
 </html>
