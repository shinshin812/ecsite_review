<?php
require('function.php');
$db = dbConnect();

    $currentPage=filter_input(INPUT_GET,"c")?:1;
    $lastPage=2;
    $perPage=2;
    $bigMove=true;
    $pager_first=false;
    $pager_last=false;
    if(ceil($perPage/2) >= $currentPage ){
    $start=1;
    $pager_first=true;
    }elseif(floor(($perPage)/2) >= $lastPage - $currentPage ){
    $start=$lastPage - $perPage +1;
    $pager_last=true;
    }else{
    $start=$currentPage-ceil($perPage/2) +1;
    }
?>
<html>
<title>商品一覧</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .color{
      border-bottom: solid 3px #87CEFA;
    }
    .largelink{
      font-size:2em;
      color: #005FFF;
    }
    .cart-position{
      margin-top: 7px;
    }
    .font{
      margin-top: 90px;
    }
</style>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand fas" href="home.php">&#xf015;</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="product_list.php">LIST
              <span class="sr-only">(current)</span>
            </a>
          </li>
            <form method="post" action="cart.php">
              <div class="cart-position">
              <input class="btn btn-primary btn-sm fas" type="submit" name="nakami" value="&#xf07a; cart">
            </div>
            </form>
            <li class="nav-item">
              <?php if(!empty($_SESSION['name'])):
                    $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
                    $stmt->execute(array($_SESSION['name'])); ?>
                <form method="post" name="form" action="favorite_list.php">
                <a class="nav-link" href="javascript:form.submit()">favorite</a>
                <?php foreach($stmt as $row): ?>
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
              </form>
            </li>
          <?php endforeach ?>
        <?php elseif(empty($_SESSION['name'])):
                $sql = "SELECT * FROM user";
                $stmt = $db->prepare($sql);
                $stmt->execute(); ?>
                <form method="post" name="form" action="favorite_list.php">
                <a class="nav-link" href="javascript:form.submit()">favorite</a>
                <?php foreach($stmt as $row): ?>
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
              </form>
            </li>
          <?php endforeach ?>
        <?php endif ?>
            <li class="nav-item">
              <?php if(!empty($_SESSION['name'])):
                    $stmt = $db->prepare("SELECT * FROM user WHERE name= ? ");
                    $stmt->execute(array($_SESSION['name'])); ?>
                <form method="post" name="form1" action="order_history.php">
                <a class="nav-link" href="javascript:form1.submit()">Order History</a>
                <?php foreach($stmt as $row): ?>
                <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
              </form>
            </li>
            <?php endforeach ?>
          <?php elseif(empty($_SESSION['name'])):
                  $sql = "SELECT * FROM user";
                  $stmt = $db->prepare($sql);
                  $stmt->execute(); ?>
              <form method="post" name="form1" action="order_history.php">
              <a class="nav-link" href="javascript:form1.submit()">Order History</a>
              <?php foreach($stmt as $row): ?>
              <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
            </form>
          </li>
          <?php endforeach ?>
          <?php endif ?>
          <li class="nav-item">
            <?php if(!empty($_SESSION['name'])): ?>
            <a class="nav-link" href="logout.php">Logout</a>
          <?php elseif(empty($_SESSION['name'])): ?>
            <a class="nav-link" href="login.php">Login</a>
          <?php endif ?>
          </li>
          <div class="dropdown">
  <button type="button" class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    MENU
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="product_list.php">LIST</a>
    <?php if(!empty($_SESSION['name'])): ?>
    <a class="dropdown-item" href="logout.php">Logout</a>
  <?php elseif(empty($_SESSION['name'])): ?>
    <a class="dropdown-item" href="login.php">Login</a>
  <?php endif ?>
  <a class="dropdown-item" href="ranking.php">ranking</a>
  <?php if(!empty($_SESSION['name'])): ?>
  <a class="dropdown-item" href="inquiry_input.php">お問い合わせ</a>
<?php endif ?>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="home.php">NIKE</a>
  </div>
</div>
      </ul>
    </div>
  </div>
</nav>
<div class="font">
  <div class="container mt-5">
    <div class="text-center">
      <?php if(!empty($_SESSION['name'])): ?>
    <h3><?php echo "こんにちは".$_SESSION['name']."様" ?></h3>
  <?php endif ?>
    <h1 class="color">商品一覧</h1>
    <?php if(isset($_POST['category'])){
          $category = $_POST['category'];
          $sql= "SELECT * FROM product WHERE category = '$category'";
          $stmt = $db->query($sql);
        }elseif(isset($_POST['search-button'])){
          // if($_POST['search'] == ""){
          //   header('location: home.php');
          //   exit();
          // }
              $category = $_POST['search'];
              $sql = "SELECT * FROM product WHERE EXISTS (SELECT * FROM product WHERE category = '$category')";
              $stmt = $db->query($sql);
              $row = $stmt->fetch(PDO::FETCH_ASSOC);
              if($row == FALSE){
                echo '<h1 class="text-center">検索対象は見つかりませんでした</h1>
                <h3>再検索</h3>
                <form method="post" action="product_list.php">
                  <div class="input-group mb-3">
                    <input class="form-control" placeholder="Search" type="text" name="search" id="hogehoge">
                    <input class="btn btn-primary fas" type="submit" name="search-button" value="&#xf002;">
                  </form>
                </div>';
              }elseif($row == TRUE){
                    $category = $_POST['search'];
                    $sql = "SELECT * FROM product WHERE category = '$category'";
                    $stmt = $db->query($sql);
                  }
                }elseif(!isset($_POST['category'])){
                  $sql= "SELECT * FROM product";
                  $stmt = $db->query($sql);
                }
                ?>
                <div class="row">
                  <div class="col-lg-12 mt-4">
                   <div class="row">
                <?php
                foreach ($stmt as $row):
                  if(!isset($category)){
                    if($currentPage == 1){
                      if(0 == strcmp($row['id'], 10)){
                        break;
                      }
                    }elseif($currentPage == 2){
                      if(9 >= $row['id']){
                        continue;
                      }
                    }
                  }
              ?>
      <div class="col-xs-12 col-lg-4 mb-4">
        <div class="card img-thumbnail">
          <a href="#">
        <img class="card-img-top" src="<?php echo "../../".$row['product_image'] ?>" onmouseover="this.src='<?php echo "../../".$row['image_detail1'] ?>'" onmouseout="this.src='<?php echo "../../".$row['product_image'] ?>'">
        </a>
         <div class="card-body">
            <h4 class="card-title">
        <?php echo $row['product_name'] ?>
      </h4>
        <h5><?php echo $row['product_introduction'] ?></h5>
        <p><?php $tax = 1.1;
                  $price = $row['price'] * $tax;
                  $product_price = number_format($price);
                  echo $product_price; ?>円(税込)</p>
        </div>
        <div class="card-footer">
        <form method="post" action="product_detail.php">
        <input class="btn btn-primary btn-block" type="submit" value="詳細">
        <input type="hidden" name="id" value="<?php echo $row['id'] ?>">
        <input type="hidden" name="product_name" value="<?php echo $row['product_name'] ?>">
        <input type="hidden" name="product_image" value="<?php echo "../../".$row['product_image'] ?>">
        <input type="hidden" name="image_detail1" value="<?php echo "../../".$row['image_detail1'] ?>">
        <input type="hidden" name="image_detail2" value="<?php echo "../../".$row['image_detail2'] ?>">
        <input type="hidden" name="image_detail3" value="<?php echo "../../".$row['image_detail3'] ?>">
        <input type="hidden" name="image_detail4" value="<?php echo "../../".$row['image_detail4'] ?>">
        <input type="hidden" name="product_introduction" value="<?php echo $row['product_introduction'] ?>">
        <input type="hidden" name="price" value="<?php echo $price ?>">
        <?php if(isset($_SESSION['name'])):
              $stmt1 = $db->prepare("SELECT * FROM user WHERE name= ? ");
              $stmt1->execute(array($_SESSION['name']));
              foreach($stmt1 as $row1): ?>
          <input type="hidden" name="user_id" value="<?php echo $row1['user_id'] ?>">
        <?php endforeach ?>
      <?php endif ?>
      </form>
    </div>
  </div>
</div>
<?php endforeach ?>
    </div>
  </div>
</div>
    <?php
    if(!isset($category)){
    for($i=$start;$i<$start+$perPage;$i++){
      if($i==$currentPage){
        print "<span class='largelink'>{$i}</span> ";
      }else{
        print "<a href='?c={$i}'>{$i}</a> ";
      }
    }
  }
     ?>
   </div>
 </div>
</div>
<?php if($row == FALSE): ?>
<footer class="py-5 bg-dark fixed-bottom">
<?php elseif($row != FALSE): ?>
<footer class="py-5 bg-dark">
<?php endif ?>
  <div class="container">
    <div class="text-center">
      <?php if(!empty($_SESSION['name'])): ?>
      <a class="text-white" href="inquiry_input.php">お問い合わせ</a>
    <?php endif ?>
    </div>
  </div>
  <p class="m-0 text-center text-white">@God Mountain</p>
  </footer>
</body>
<script>
$(function() {
  // 入力補完候補の単語リスト
  var wordlist = [
    "sneakers",
    "fruit",
    "vegetable"
  ];

  // 入力補完を実施する要素に単語リストを設定
  $( "#hogehoge" ).autocomplete({
    source: wordlist
  });
});
</script>
</html>
