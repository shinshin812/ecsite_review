<?php
require('function.php');
$db = dbConnect();
$userId = $_POST['user_id'];
 ?>
<html>
<title>お気に入りリスト</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .color{
      border-bottom: solid 3px #87CEFA;
    }
    .font{
      margin-top: 90px;
    }
</style>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand fas" href="home.php">&#xf015;</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="product_list.php">LIST
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout.php">Logout</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

<div class="font">
  <div class="container mt-5">
    <div class="text-center">
      <div class="row">
        <h1>お気に入りリスト</h1>
  <?php if(!empty($_SESSION['name'])):
        $sql = "SELECT * FROM favorite WHERE user_id = '$userId'";
        $stmt = $db->query($sql);
        foreach ($stmt as $row): ?>
<table class="table">
  <tbody>
		<tr>
			<td width="200" height="200" rowspan="3"><img src="<?php echo $row['favorite_image'] ?>" width="200" height="200"></td>
      <ul>
			<td><p class="color">商品名</p><li><?php echo $row['favorite_name'] ?></li></td>
			<td><form method="post" action="cart.php">
        <input class="btn btn-primary btn-block" type="submit" name="submit" value="カートに入れる">
        <input type="hidden" name="id" value="<?php echo $row['favorite_id'] ?>">
        <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
        <input type="hidden" name="product_name" value="<?php echo $row['favorite_name'] ?>">
        <input type="hidden" name="product_image" value="<?php echo $row['favorite_image'] ?>">
        <input type="hidden" name="product_introduction" value="<?php echo $row['favorite_introduction'] ?>">
        <input type="hidden" name="price" value="<?php echo $row['favorite_price'] ?>">
        <select name="number">
        <?php for($x=1; $x<=9; $x++){
          echo "<option>{$x}</option>";
        } ?></select>
      </form></td>
		</tr>
		<tr>
			<td><p class="color">詳細</p><li><?php echo $row['favorite_introduction'] ?></li></td>
      <form method="post" action="delete.php">
			<td width="300"><input class="btn btn-danger btn-block" type="submit" name="delete" value="削除" onclick="return confirm('本当に削除してよろしいですか？')"></td>
      <input type="hidden" name="id" value="<?php echo $row['favorite_id'] ?>">
      <input type="hidden" name="user_id" value="<?php echo $row['user_id'] ?>">
     </form>
		</tr>
		<tr>
			<td><p class="color">値段</p><li><?php $price = number_format($row['favorite_price']);
              echo $price ?>円(税込)</li></td>
      </ul>
    </tr>
	</tbody>
  </table>
  <?php endforeach ?>
      </div>
    </div>
  </div>
</div>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
<?php elseif(empty($_SESSION['name'])):
  header('location: login.php');
  exit();
  endif ?>
<footer class="py-5 bg-dark fixed-bottom">
  <div class="container">
    <div class="text-center">
      <?php if(!empty($_SESSION['name'])): ?>
      <a class="text-white" href="inquiry_input.php">お問い合わせ</a>
    <?php endif ?>
    </div>
  </div>
  <p class="m-0 text-center text-white">@God Mountain</p>
  </footer>
</body>
</html>
