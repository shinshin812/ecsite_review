<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<?php
session_start();
// db
function dbConnect(){
  $dsn = "mysql:dbname=EC1;host=localhost;charset=utf8mb4";
  $user = "root";
  $password = "";

  $db = new PDO($dsn, $user, $password);
  $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  return $db;
}
// user_register
function displayName(){
  if(isset($_POST['name'])){
    echo $_POST['name'];
  }
}
function displayEmail(){
  if(isset($_POST['email'])){
    echo $_POST['email'];
  }
}
function displayPassword(){
  if(isset($_POST['password'])){
  echo $_POST['password'];
  }
}
function displayAddress(){
  if(isset($_POST['address'])){
    echo $_POST['address'];
  }
}
function displayCredit(){
  if(isset($_POST['credit'])){
    echo $_POST['credit'];
  }
}
function checkName(){
  if(isset($_POST['name']) && $_POST['name'] == ""){
    echo "※名前が未入力です"."</br>";
  }
}
function checkEmail(){
  if(isset($_POST['email']) && $_POST['email'] == ""){
    echo "※メールアドレスが未入力です"."</br>";
  }
}
function checkPassword(){
  if(isset($_POST['password']) && $_POST['password'] == ""){
    echo "※パスワードが未入力です"."</br>";
  }
}
function checkAddress(){
  if(isset($_POST['address']) && $_POST['address'] == ""){
    echo "※住所が未入力です"."</br>";
  }
}
function checkCredit(){
  if(isset($_POST['credit']) && $_POST['credit'] == ""){
    echo "※クレジットカード情報が未入力です";
  }
}
function userCheckOk(){
  if(isset($_POST['name'])){
    if($_POST['name'] != "" && $_POST['email'] != ""){
      if($_POST['password'] != "" && $_POST['address'] != ""){
        if($_POST['credit'] != ""){
          $_SESSION = $_POST;
          header('location: user_confirm.php');
          exit();
        }
      }
    }
  }
}

// admin_register
function adminCheckOk(){
  if(isset($_POST['name'])){
    if($_POST['name'] != "" && $_POST['email'] != ""){
      if($_POST['password'] != ""){
          $_SESSION = $_POST;
          header('location: admin_confirm.php');
          exit();
      }
    }
  }
}

// product_register
function displayProductName(){
  if(isset($_POST['product_name'])){
    echo $_POST['product_name'];
  }
}
function displayCategory(){
  if(isset($_POST['category'])){
    echo $_POST['category'];
  }
}
function displayProductImage(){
  if(isset($_POST['product_image'])){
    echo $_POST['product_image'];
  }
}
function displayProductIntroduction(){
  if(isset($_POST['product_introduction'])){
    echo $_POST['product_introduction'];
  }
}
function displayPrice(){
  if(isset($_POST['price'])){
    echo $_POST['price'];
  }
}
function checkProductName(){
  if(isset($_POST['product_name']) && $_POST['product_name'] == ""){
    echo "※商品名が未入力です"."</br>";
  }
}
function checkCategory(){
  if(isset($_POST['category']) && $_POST['category'] == ""){
    echo "※カテゴリーが未入力です"."</br>";
  }
}
function checkProductIntroduction(){
  if(isset($_POST['product_introduction']) && $_POST['product_introduction'] == ""){
    echo "※紹介文が未入力です"."</br>";
  }
}
function checkPrice(){
  if(isset($_POST['price']) && $_POST['price'] == ""){
    echo "※値段が未入力です";
  }
}
function checkOk(){
  if(isset($_POST['product_name'])){
    if($_POST['product_name'] != "" && $_POST['category'] != ""){
      if($_POST['product_introduction'] != "" && $_POST['price']){
        $tempfile = $_FILES['product_image']['tmp_name'];
        $filemove = '/Applications/XAMPP/htdocs/img/' . $_FILES['product_image']['name'];
        move_uploaded_file($tempfile , $filemove );
        $tempfile1 = $_FILES['image_detail1']['tmp_name'];
        $filemove1 = '/Applications/XAMPP/htdocs/img/' . $_FILES['image_detail1']['name'];
        move_uploaded_file($tempfile1 , $filemove1 );
        $tempfile2 = $_FILES['image_detail2']['tmp_name'];
        $filemove2 = '/Applications/XAMPP/htdocs/img/' . $_FILES['image_detail2']['name'];
        move_uploaded_file($tempfile2 , $filemove2 );
        $tempfile3 = $_FILES['image_detail3']['tmp_name'];
        $filemove3 = '/Applications/XAMPP/htdocs/img/' . $_FILES['image_detail3']['name'];
        move_uploaded_file($tempfile3 , $filemove3 );
        $tempfile4 = $_FILES['image_detail4']['tmp_name'];
        $filemove4 = '/Applications/XAMPP/htdocs/img/' . $_FILES['image_detail4']['name'];
        move_uploaded_file($tempfile4 , $filemove4 );
        $_SESSION=$_POST;
        $_SESSION['product_image']['name']=$_FILES['product_image']['name'];
        $_SESSION['image_detail1']['name']=$_FILES['image_detail1']['name'];
        $_SESSION['image_detail2']['name']=$_FILES['image_detail2']['name'];
        $_SESSION['image_detail3']['name']=$_FILES['image_detail3']['name'];
        $_SESSION['image_detail4']['name']=$_FILES['image_detail4']['name'];
        header('location: product_confirm.php');
        exit();
      }
    }
  }
}

// inquiry
function displayMessage(){
  if(isset($_POST['message'])){
    echo $_POST['message'];
  }
}
function displayTitle(){
  if(isset($_POST['title'])){
    echo $_POST['title'];
  }
}
function checkMessage(){
  if(isset($_POST['message']) && $_POST['message'] == ""){
    echo "※本文が未入力です"."</br>";
  }
}
function checkTitle(){
  if(isset($_POST['title']) && $_POST['title'] == ""){
    echo "※題名が未入力です"."</br>";
  }
}

// review
function displayNickname(){
  if(isset($_POST['nickname'])){
    echo $_POST['nickname'];
  }
}
function displayComment(){
  if(isset($_POST['comment'])){
    echo $_POST['comment'];
  }
}
function checkNickname(){
  if(isset($_POST['nickname']) && $_POST['nickname'] == ""){
    echo "※ニックネームが未入力です";
  }
}
function checkComment(){
  if(isset($_POST['comment']) && $_POST['comment'] == ""){
    echo "※コメントが未入力です";
  }
}
function checkCountNickName(){
if(isset($_POST['nickname']) && mb_strlen($_POST['nickname'])>10){
echo "10文字以内でお願いします";
  }
}
function checkCountComment(){
if(isset($_POST['comment']) && mb_strlen($_POST['comment'])>30){
echo "30文字以内でお願いします";
  }
}

?>
