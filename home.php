<?php
require('function.php');
$db = dbConnect();

$sql = "SELECT * FROM product ORDER BY id DESC limit 2";
$stmt = $db->prepare($sql);
$stmt->execute();
 ?>
<html>
<title>ホーム</title>
<style>
    body{
      background: linear-gradient(-135deg, #E4A972, #9941D8) fixed;
      color: #FFFFFF;
    }
    .font{
      font-family: "Comic Sans MS";
      margin-top: 90px;
    }
</style>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand fas" href="#">&#xf015;</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <!-- <form class="form-inline" method="post" action="product_list.php">
         </form> -->
          <li class="nav-item">
            <?php if(empty($_SESSION['name'])): ?>
            <a class="nav-link" href="user_register.php">sign up</a>
          <?php endif ?>
          </li>
          <li class="nav-item">
            <?php if(empty($_SESSION['name'])): ?>
            <a class="nav-link" href="login.php">login</a>
          <?php endif ?>
          </li>
          <li class="nav-item">
            <?php if(!empty($_SESSION['name'])): ?>
            <a class="nav-link" href="logout.php">Logout</a>
          <?php endif ?>
          </li>
          <div class="dropdown">
  <button type="button" class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    MENU
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="product_list.php">LIST</a>
    <?php if(!empty($_SESSION['name'])): ?>
    <a class="dropdown-item" href="logout.php">Logout</a>
  <?php elseif(empty($_SESSION['name'])): ?>
    <a class="dropdown-item" href="login.php">Login</a>
  <?php endif ?>
  <?php if(empty($_SESSION['name'])): ?>
  <a class="dropdown-item" href="user_register.php">sign up</a>
<?php endif ?>
<a class="dropdown-item" href="ranking.php">ranking</a>
  <?php if(!empty($_SESSION['name'])): ?>
  <a class="dropdown-item" href="inquiry_input.php">お問い合わせ</a>
<?php endif ?>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="home.php">NIKE</a>
  </div>
    </div>
        </ul>
      </div>
    </div>
  </nav>

  <div class="font">
    <div class="container mt-5">
      <div class="row">
        <div class="col-lg-12">
          <form method="post" action="product_list.php">
            <div class="input-group mb-3">
              <input class="form-control" placeholder="Search" type="text" name="search" id="hogehoge">
              <input class="btn btn-primary fas" type="submit" name="search-button" value="&#xf002;">
            </form>
          </div>
          <h1>Shopping Site</h1>
          <h2 class="subtitle">all item</h2>
          <div class="card img-thumbnail">
          <img class="card-img-top" width="300px" src="<?php echo "../../img/0320_snkrs_app_cdp_p1_dt_v2.png" ?>">
        </div>
          <form method="post" action="product_list.php">
            <div class="form-group">
              <input class="btn btn-primary btn-lg btn-block" type="submit" name="submit" value="全ての商品一覧を見る ➡︎">
            </div>
          </form>
        </div>
        <?php foreach($stmt as $row): ?>
          <div class="col-lg-6">
            <h2 class="subtitle">New product</h2>
            <div class="card img-thumbnail">
              <img class="card-img-top" src="<?php echo "../../".$row['product_image'] ?>">
            </div>
            <form method="post" action="product_detail.php">
              <input type="hidden" name="id" value="<?php echo $row['id'] ?>">
              <input type="hidden" name="product_name" value="<?php echo $row['product_name'] ?>">
              <input type="hidden" name="product_image" value="<?php echo "../../".$row['product_image'] ?>">
              <input type="hidden" name="image_detail1" value="<?php echo "../../".$row['image_detail1'] ?>">
              <input type="hidden" name="image_detail2" value="<?php echo "../../".$row['image_detail2'] ?>">
              <input type="hidden" name="image_detail3" value="<?php echo "../../".$row['image_detail3'] ?>">
              <input type="hidden" name="image_detail4" value="<?php echo "../../".$row['image_detail4'] ?>">
              <input type="hidden" name="product_introduction" value="<?php echo $row['product_introduction'] ?>">
              <?php $tax = 1.1;
                        $price = $row['price'] * $tax;
                        $product_price = number_format($price); ?>
              <input type="hidden" name="price" value="<?php echo $price ?>">
              <div class="form-group">
                <input class="btn btn-primary btn-lg btn-block" type="submit" name="submit" value="新着商品を見る">
              </div>
            </form>
          </div>
        <?php endforeach ?>
        <div class="col-lg-4">
          <h2 class="subtitle">sneakers</h2>
          <div class="card img-thumbnail">
          <img class="card-img-top" src="<?php echo "../../img/38535244_239903629995892_7034535374282031104_n.jpg" ?>">
        </div>
          <form method="post" action="product_list.php">
            <?php $sql = "SELECT * FROM product WHERE category = 'sneakers'";
                  $stmt = $db->query($sql);
                  foreach ($stmt as $row): ?>
                  <input type="hidden" name="category" value="<?php echo $row['category'] ?>">
                <?php endforeach ?>
            <div class="form-group">
              <input class="btn btn-primary btn-lg btn-block" type="submit" name="submit" value="商品一覧を見る">
            </div>
          </form>
        </div>
        <div class="col-lg-4">
          <h2 class="subtitle">fruit</h2>
          <div class="card img-thumbnail">
          <img class="card-img-top" src="<?php echo "../../img/illustrain04-food13.png" ?>">
        </div>
          <form method="post" action="product_list.php">
            <?php $sql = "SELECT * FROM product WHERE category = 'fruit'";
                  $stmt = $db->query($sql);
                  foreach ($stmt as $row): ?>
                  <input type="hidden" name="category" value="<?php echo $row['category'] ?>">
                <?php endforeach ?>
            <div class="form-group">
              <input class="btn btn-primary btn-lg btn-block" type="submit" name="submit" value="商品一覧を見る">
            </div>
          </form>
        </div>
        <div class="col-lg-4">
          <h2 class="subtitle">vegetable</h2>
          <div class="card img-thumbnail">
          <img class="card-img-top" src="<?php echo "../../img/6607.png" ?>">
        </div>
          <form method="post" action="product_list.php">
            <?php $sql = "SELECT * FROM product WHERE category = 'vegetable'";
                  $stmt = $db->query($sql);
                  foreach ($stmt as $row): ?>
                  <input type="hidden" name="category" value="<?php echo $row['category'] ?>">
                <?php endforeach ?>
            <div class="form-group">
              <input class="btn btn-primary btn-lg btn-block" type="submit" name="submit" value="商品一覧を見る">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <footer class="py-5 bg-dark">
    <div class="container">
      <div class="text-center">
        <?php if(!empty($_SESSION['name'])): ?>
        <a class="text-white" href="inquiry_input.php">お問い合わせ</a>
      <?php endif ?>
      </div>
    </div>
    <p class="m-0 text-center text-white">@God Mountain</p>
    </footer>
</body>
<script>
$(function() {
  // 入力補完候補の単語リスト
  var wordlist = [
    "sneakers",
    "fruit",
    "vegetable"
  ];

  // 入力補完を実施する要素に単語リストを設定
  $( "#hogehoge" ).autocomplete({
    source: wordlist
  });
});
</script>
</html>
