<?php
require('function.php');

$db = dbConnect();
$userName = $_SESSION['name'];
$userEmail = $_SESSION['email'];
$hash = password_hash("{$_SESSION['password']}", PASSWORD_DEFAULT);
$userAddress = $_SESSION['address'];
$userCredit = $_SESSION['credit'];

$sql="INSERT INTO user (name, email, password, address, credit) VALUES (:userName, :userEmail, :hash, :userAddress, :userCredit)";
$stmt = $db->prepare($sql);
$stmt->bindParam(':userName',$userName, PDO::PARAM_STR);
$stmt->bindParam(':userEmail',$userEmail, PDO::PARAM_STR);
$stmt->bindParam(':hash',$hash, PDO::PARAM_STR);
$stmt->bindParam(':userAddress',$userAddress, PDO::PARAM_STR);
$stmt->bindParam(':userCredit',$userCredit, PDO::PARAM_STR);
$stmt->execute();
?>
<html>
<title>ユーザー登録完了</title>
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .form-wrapper {
      background: #fafafa;
      margin: 3em auto;
      padding: 20 20px;
      width: 500px;
    }
    .text-center{
        font-weight: bold;
        font-size: 20px;
        margin: 70px 320px;
    }
</style>
<body>
  <div class="container">
    <div class="row">
      <div class="text-center">
        <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
  <h2>登録完了</h2>
  <div class="form-group">
    名前</br>
  <?php echo $_SESSION['name'] ?>
  </div>
  <div class="form-group">
    メールアドレス</br>
  <?php echo $_SESSION['email'] ?>
  </div>
  <div class="form-group">
    パスワード</br>
  <?php echo $_SESSION['password'] ?>
  </div>
  <div class="form-group">
    住所</br>
  <?php echo $_SESSION['address'] ?>
  </div>
  <div class="form-group">
    クレジットカード番号</br>
  <?php echo $_SESSION['credit'] ?>
  </div>
  <a href="http://localhost/task/ecsite1/login.php">ログインへ</a>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
