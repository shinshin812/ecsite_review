<?php
require('function.php');
$db = dbConnect();

$productName = $_SESSION['product_name'];
$category = $_SESSION['category'];
$filename = 'img/' . $_SESSION['product_image']['name'];
$filename1 = 'img/' . $_SESSION['image_detail1']['name'];
$filename2 = 'img/' . $_SESSION['image_detail2']['name'];
$filename3 = 'img/' . $_SESSION['image_detail3']['name'];
$filename4 = 'img/' . $_SESSION['image_detail4']['name'];
$productPrice = $_SESSION['price'];
$productIntroduction = $_SESSION['product_introduction'];

$sql="INSERT INTO product (product_name, category, product_image, image_detail1, image_detail2, image_detail3, image_detail4, price, product_introduction) VALUES (:productName, :category, :filename, :filename1, :filename2, :filename3, :filename4, :productPrice, :productIntroduction)";
$stmt = $db->prepare($sql);
$stmt->bindParam(':productName',$productName, PDO::PARAM_STR);
$stmt->bindParam(':category',$category, PDO::PARAM_STR);
$stmt->bindValue(':filename',$filename, PDO::PARAM_STR);
$stmt->bindValue(':filename1',$filename1, PDO::PARAM_STR);
$stmt->bindValue(':filename2',$filename2, PDO::PARAM_STR);
$stmt->bindValue(':filename3',$filename3, PDO::PARAM_STR);
$stmt->bindValue(':filename4',$filename4, PDO::PARAM_STR);
$stmt->bindParam(':productPrice',$productPrice, PDO::PARAM_INT);
$stmt->bindParam(':productIntroduction',$productIntroduction, PDO::PARAM_STR);
$stmt->execute();
?>
<html>
<title>商品登録完了</title>
<style>
      body{
        background: #e9e9e9;
        color: #5e5e5e;
      }
      .form-wrapper {
        background: #fafafa;
        margin: 3em auto;
        padding: 20 20px;
        width: 800px;
      }
    .text-center{
        font-weight: bold;
        font-size: 17px;
        margin: 30px 150px;
    }
</style>
<body>
  <div class="container">
    <div class="row">
      <div class="text-center">
        <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
  <h2>登録完了</h2>
  <div class="form-group">
    商品名</br>
  <?php echo $_SESSION['product_name'] ?>
  </div>
  <div class="form-group">
    画像</br>
    <img src="<?php echo "../../img/".$_SESSION['product_image']['name'] ?>">
  </div>
  <div class="form-group">
    画像詳細</br>
    <img src="<?php echo "../../img/".$_SESSION['image_detail1']['name'] ?>">
    <img src="<?php echo "../../img/".$_SESSION['image_detail2']['name'] ?>">
    <img src="<?php echo "../../img/".$_SESSION['image_detail3']['name'] ?>">
    <img src="<?php echo "../../img/".$_SESSION['image_detail4']['name'] ?>">
  </div>
  <div class="form-group">紹介文</div>
  <?php echo $_SESSION['product_introduction'] ?>
  <div class="form-group">
    値段</br>
  <?php echo $_SESSION['price'] ?>
  </div>
  <input type="button" value="戻る" onclick="location.href='product_register.php'">
        </div>
      </div>
    </div>
  </div>
</body>
</html>
